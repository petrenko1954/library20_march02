#library20
Rails.application.routes.draw do
  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
#March_08  root 'users#show'

root             'static_pages#home'

  # Example of regular route:
  get 'attach_files' => 'migration#attach_files'
  post 'attach_files' => 'migration#attach_files_action'

  post 'api/reviews_by_chapter' => 'categories#reviews_by_chapter'
  post 'api/sections_by_review' => 'categories#sections_by_review'
  post 'api/subsections_by_section' => 'categories#subsections_by_section'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  resources :articles
  resources :folders
  resources :reports

 
  
end
